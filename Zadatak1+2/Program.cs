﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        

        static void Main(string[] args)
        {
            Box box = new Box("Pokloni");
            Product product = new Product("Xbox1", 100, 6);
            Product pro = new Product("Call of duty", 37, 3);
            box.Add(product);
            box.Add(pro);


            ShippingService price = new ShippingService(4);
            double w = 0;
            List<IShipable> list = new List<IShipable>();
            list.Add(box);
            foreach (IShipable ship in list)
            {

                w += ship.Weight;

                Console.WriteLine(ship.Description());

            }

            Console.WriteLine(price.ToString() + price.Price(w) + " hrk");

        }
    }
}
